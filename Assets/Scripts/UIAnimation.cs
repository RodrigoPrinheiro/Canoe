﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAnimation : MonoBehaviour
{
    private RectTransform _transform;
    [SerializeField] private float _translation;
    [SerializeField] private bool _vertical;
    [SerializeField] private float _animationSpeed;
    private Vector3 _finalPoint;
    private Vector3 _initialPosition;
    private void Awake()
    {
        _transform = GetComponent<RectTransform>();
    }
    private void Start() 
    {
        _initialPosition = _transform.anchoredPosition;
        _finalPoint = _initialPosition;
        if (_vertical)
            _finalPoint.y += _translation;
        else
            _finalPoint.x += _translation;
    }
    public void Boing()
    {
        LeanTween.move(_transform, _finalPoint,_animationSpeed).setEaseOutCirc();
    }

    public void UnBoing()
    {
        LeanTween.move(_transform, _initialPosition, _animationSpeed).setEaseOutCirc();
    }
}
