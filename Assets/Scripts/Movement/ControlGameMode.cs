﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlGameMode : Singleton<ControlGameMode>
{
    public bool mouseMovement {get; set;}
    public bool rotationAround {get; set;}
}
