using UnityEngine;

namespace Movement
{
    public class CameraMovement : MonoBehaviour
    {
        private const float MAX_ANGLE_X = 45f;
        private const float MAX_ANGLE_Y = 90f;
        [SerializeField] float _mouseSensitivity = 5f;
        [SerializeField] Transform _target;

        private Vector2 _inputVector;
        private float _rotationX;
        private float _rotationY;
        private bool _rotationDirection;
        private Collider _col;

        private void Awake() {
            _rotationX = transform.eulerAngles.x;
            _rotationY = transform.eulerAngles.y;
        }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update()
        {
            if (ControlGameMode.Instance.mouseMovement)
            {
                _inputVector.Set(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                _inputVector *= _mouseSensitivity * Time.deltaTime;
            }
            else
            {
                // While button pressed rotate over Y axis
                _inputVector.Set(Input.GetKey(KeyCode.Space) ? 1 : 0, 0);
                if (!ControlGameMode.Instance.rotationAround)
                {
                    if (!_rotationDirection)
                        _inputVector = -_inputVector;
                }
            }

            _rotationX -= _inputVector.y;
            _rotationX = Mathf.Clamp(_rotationX, -MAX_ANGLE_X, MAX_ANGLE_X);
            _rotationY += _inputVector.x;

            if (!ControlGameMode.Instance.rotationAround)
            {
                // Raycast stuff
                if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 2f, LayerMask.GetMask("CameraBlockers")))
                {
                    if (_col != hit.collider)
                        _rotationDirection = !_rotationDirection;
                    
                    _col = hit.collider;
                }
                else
                {
                    _col = null;
                }
            }

            transform.rotation = Quaternion.Euler(_rotationX, _rotationY, 0f);
            transform.position = _target.position;

        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position, transform.forward * 2f);
            Gizmos.DrawRay(_target.position, _target.forward * 2f);
        }

        float ClampAngle(float angle, float from, float to)
        {
            // accepts e.g. -80, 80
            if (angle < 0f) angle = 360 + angle;
            if (angle > 180f) return Mathf.Max(angle, 360 + from);
            return Mathf.Min(angle, to);
        }
    }
}