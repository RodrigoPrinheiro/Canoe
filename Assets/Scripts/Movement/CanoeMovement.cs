﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Movement
{
    public class CanoeMovement : MonoBehaviour
    {
        [SerializeField] private float _speed = 1;
        [SerializeField] private float _angularSpeed = 1;
        [SerializeField] private float _moveAcceleration = 1;
        [SerializeField] private float _angularAcceleration = 1;
        [SerializeField] private Transform _mainCamera = null;

        private Vector3 _currentVelocity;
        private float _currentAngularVel;
        private Rigidbody _rb;

        // Debug
        private Vector3 _cameraDir;
        private Vector3 _lookDir;
        private Vector3 _input;
        private float _startY;

        public bool LockMovement {get; set;}

        // Start is called before the first frame update
        void Start()
        {
            _currentVelocity = Vector3.zero;
            _startY = transform.position.y;
        }

        private void Update()
        {
            if (_input.magnitude > 0.1f)
                UpdateRotation();
            UpdatePosition();
        }

        private void LateUpdate() {
            Vector3 finalPosition = transform.position;
            finalPosition.y = _startY;
            transform.position = finalPosition;
        }

        private void UpdateRotation()
        {
            _lookDir = _mainCamera.forward;
            _cameraDir = Vector3.ProjectOnPlane(_lookDir, transform.up);
            float angToTarget = Vector3.SignedAngle(transform.forward, _cameraDir, transform.up);

            float targetAngularVelocity = 0;
            if (Mathf.Abs(angToTarget) > 3)
            {
                if (angToTarget > 0)
                    targetAngularVelocity = _angularSpeed;
                else
                    targetAngularVelocity = -_angularSpeed;
            }

            _currentAngularVel = Mathf.Lerp(_currentAngularVel, targetAngularVelocity, 1 - Mathf.Exp(-_angularAcceleration * Time.deltaTime));
            transform.Rotate(0, Time.deltaTime * _currentAngularVel, 0, Space.World);
        }

        private void UpdatePosition()
        {
            Vector3 targetVelocity = Vector3.zero;
            _input = new Vector3(0, 0, Input.GetKey(KeyCode.W) ? 1 : 0);

            targetVelocity = _speed * _input.z * transform.forward.normalized;
            targetVelocity += _speed * _input.x * transform.right.normalized;

            _currentVelocity = Vector3.Lerp(_currentVelocity, targetVelocity,
                 1 - Mathf.Exp(-_moveAcceleration * Time.deltaTime));

            transform.position += _currentVelocity * Time.deltaTime;
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawRay(transform.position, transform.forward * 3);
            Gizmos.DrawRay(transform.position, _cameraDir * 3);
        }
    }
}
