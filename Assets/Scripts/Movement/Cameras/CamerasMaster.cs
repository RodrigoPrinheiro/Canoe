﻿using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System.Linq;
using System;

namespace Movement.Cameras
{
    public class CamerasMaster : MonoBehaviour
    {
        private List<CameraHandler> _cameras;
        private CameraHandler _activeCamera;
        public CameraType ActiveType => _activeCamera.Type;

        private void Start()
        {
            _cameras = new List<CameraHandler>();
            _cameras = GetComponentsInChildren<CameraHandler>().ToList();
            SetNewCamera(CameraType.FRONT);
        }

        public void SetNewCamera(CameraType type, Transform target = null, Action onSwitch = null)
        {
            for(int i = 0; i < _cameras.Count; i++)
            {
                if (!_cameras[i].Type.Equals(type))
                {
                    _cameras[i].SetActive(false);
                }
                else
                {
                    _cameras[i].SetActive(true);

                    _activeCamera = _cameras[i];
                }
            }

            if (target != null) _activeCamera.SetNewTarget(target);
            onSwitch?.Invoke();
        }
    }
}
