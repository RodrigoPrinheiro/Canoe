﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlSettingsHelper : MonoBehaviour
{
    private Toggle _toggle;
    private void Start() {
        _toggle = GetComponent<Toggle>();
    }
    public void SetMouseMovement(bool value)
    {
        ControlGameMode.Instance.mouseMovement = _toggle.isOn;
        Debug.Log(_toggle.isOn);
    }

    public void SetRotateAround(bool value)
    {
        Debug.Log(_toggle.isOn);
        ControlGameMode.Instance.rotationAround = _toggle.isOn;
    }
}
