﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterTrigger : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.Events.UnityEvent _onEnterEvent;
    private void OnTriggerEnter(Collider other) 
    {
        _onEnterEvent?.Invoke();
    }
}
