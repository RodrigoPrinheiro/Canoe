﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioObject : AudioPlayer
{
    [SerializeField] AudioClip _clip;
    public override void PlaySound()
    {
        AudioManager.Instance.Play(uniqueID, _clip, transform);
    }
}
