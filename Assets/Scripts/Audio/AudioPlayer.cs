﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AudioPlayer : MonoBehaviour
{
    static int ID;
    protected int uniqueID;
    protected virtual void Awake() 
    {
        ID++;
        uniqueID = ID;
    }

    public abstract void PlaySound();
}
