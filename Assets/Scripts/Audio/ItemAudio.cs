using UnityEngine;
using Inventory.Items;

public class ItemAudio : AudioPlayer
{
    [SerializeField] AudioClip _itemPickUp;
    private Item item;
    protected override void Awake() 
    {
        base.Awake();
        item = GetComponent<Item>();
    }

    private void OnEnable() 
    {
        item.onItemPickUp += PlaySound;
    }

    private void OnDisable() 
    {
        item.onItemPickUp -= PlaySound;
    }

    public override void PlaySound()
    {
        AudioManager.Instance.Play(uniqueID, _itemPickUp, transform);
    }
}