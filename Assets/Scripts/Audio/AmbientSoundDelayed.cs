﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSoundDelayed : AudioPlayer
{
    [SerializeField] AudioClip ambientClip;
    [SerializeField, Range(0f, 1f)] float volume;
    public override void PlaySound()
    {
        AudioManager.Instance.PlayAMBX(uniqueID, ambientClip, transform, volume, false);
    }

    private void OnDisable() 
    {
        StopAllCoroutines();
    }

    private void Start()
    {
        StartCoroutine(PlayMultipleDelayed());
    }

    private IEnumerator PlayMultipleDelayed()
    {
        while(true)
        {
            yield return new WaitForSeconds(ambientClip.length + Random.Range(1f, 5f));
            PlaySound();
        }
    }
}
