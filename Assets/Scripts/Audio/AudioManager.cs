﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioManager : ScriptableObjectSingleton<AudioManager>
{
    [SerializeField] private AudioSource _sfxSourcePrefab;
    [SerializeField] private AudioSource _musicSourcePrefab;

    private Hashtable sources;
    private AudioSource music;

    public void Play(int ID, AudioClip clip, Transform position, float volume = 1.0f)
    {
        if (sources == null) InitAudioSources();
        AudioSource availableSource = GetAvailableSource(ID);

        if (position)
            availableSource.transform.position = position.position;
        availableSource.PlayOneShot(clip, volume);
    }

    public void PlayMX(AudioClip musicClip)
    {
        if (!music) GameObject.Instantiate(_musicSourcePrefab, Vector3.zero, Quaternion.identity);

        music.clip = musicClip;
        music.Play();
    }

    public void PlayAMBX(int ID, AudioClip ambientClip, Transform position, float volume = 1f, bool loop = true)
    {
        if (sources == null) InitAudioSources();
        AudioSource availableSource = GetAvailableSource(ID);
        
        availableSource.transform.parent = position;
        availableSource.transform.localPosition = Vector3.zero;

        availableSource.clip = ambientClip;
        availableSource.volume = volume;
        availableSource.loop = loop;
        availableSource.Play();
    }

    private void InitAudioSources()
    {
        sources = new Hashtable();
    }

    private AudioSource GetAvailableSource(int ID)
    {
        if (sources.ContainsKey(ID))
        {
            return sources[ID] as AudioSource;
        }

        AudioSource availableSource = GameObject.
            Instantiate(_sfxSourcePrefab, Vector3.zero, Quaternion.identity);

        sources.Add(ID, availableSource);
        return availableSource;
    }
}