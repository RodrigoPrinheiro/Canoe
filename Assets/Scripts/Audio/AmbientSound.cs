﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientSound : AudioPlayer
{
    [SerializeField] AudioClip ambientClip;
    [SerializeField, Range(0f, 1f)] float volume;
    public override void PlaySound()
    {
        AudioManager.Instance.PlayAMBX(uniqueID, ambientClip, transform, volume);
    }

    private void Start() 
    {
        PlaySound();
    }
}
