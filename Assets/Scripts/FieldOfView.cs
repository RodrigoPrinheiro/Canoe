﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FieldOfView : MonoBehaviour
{
    [SerializeField] private float _viewRadius = 5f;
    [Range(0, 360)]
    [SerializeField] private float _viewAngle = 45f;

    [SerializeField] private LayerMask _targetMask = default;
    [SerializeField] private LayerMask _obstacleMask = default;
    private List<Transform> _inView;

    public float Radius => _viewRadius;
    public float Angle => _viewAngle;
    public List<Transform> ViewObjects => _inView;
    public Action<Transform> onObjectView;
    public Action<Transform> onObjectExit;

    private void Awake() 
    {
        _inView = new List<Transform>();
    }

    private void Start() 
    {
        WaitForSeconds delay = new WaitForSeconds(1f);
        StartCoroutine(View(delay));
    }

    private IEnumerator View(WaitForSeconds delay)
    {
        while(true)
        {
            yield return delay;
            FindVisibleTargets();
        }
    }

    public void FindVisibleTargets()
    {
        for (int i = 0; i < _inView.Count; i++)
        {
            onObjectExit?.Invoke(_inView[i]);
        }

        _inView.Clear();
        Collider[] targets = Physics.OverlapSphere(transform.position, _viewRadius, _targetMask);

        for (int i = 0; i < targets.Length; i++)
        {
            Transform target = targets[i].transform;
            Vector3 dir = (target.position - transform.position).normalized;
            if (Vector3.Angle(transform.forward, dir) < _viewAngle / 2)
            {
                float distanceToTarget = Vector3.Distance(transform.position, target.position);
                if (!Physics.Raycast(transform.position, dir, distanceToTarget, _obstacleMask))
                {
                    onObjectView?.Invoke(target);
                    _inView.Add(target);
                }
            }
        }
    }

    public bool Contains(Transform t)
    {
        return _inView.Contains(t);
    }

    public Vector3 DirFromAngle(float angle, bool angleIsGlobal = false)
    {
        if (!angleIsGlobal) angle += transform.eulerAngles.y;
        return new Vector3(Mathf.Sin(angle * Mathf.Deg2Rad), 0, Mathf.Cos(angle * Mathf.Deg2Rad));
    }
}
