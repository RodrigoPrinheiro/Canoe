﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory.Items
{
    public interface IItemActivatable
    {
        void Activate();
        int IDRequired {get;}
    }
}
