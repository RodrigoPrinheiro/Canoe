using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

namespace Inventory.Items
{
    public class Item : MonoBehaviour
    {
        public const string ITEM_LAYER = "Items";
        [SerializeField] private string _itemName;
        [SerializeField] private ItemInfo _item;
        public string ItemName => _itemName;
        public ItemInfo Info => _item;

        private void Awake()
        {
            gameObject.layer = LayerMask.NameToLayer(ITEM_LAYER);
        }

        public void PickUp()
        {
            onItemPickUp?.Invoke();
            onPickUp.Invoke();
            
            // Scale the item to 0 and destroy it
            gameObject.LeanScale(Vector3.zero, 0.2f).setEaseOutBounce().
                setOnComplete(() => Destroy(gameObject));
        }

        public System.Action onItemPickUp;
        public UnityEvent onPickUp;
    }
}