﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory.Items
{
    public class ItemChecker : MonoBehaviour
    {
        [SerializeField] private float _checkRange;
        [SerializeField] private LayerMask _pickUpMask;
        private WaitForSeconds _wait;
        private IItemActivatable _activatable;
        private Coroutine _checkRoutine;

        public float Radius => _checkRange;
        
        private void Awake()
        {
            _activatable = GetComponent<IItemActivatable>();
            Debug.Log(_activatable);
            _wait = new WaitForSeconds(.2f);
        }

        private void Start() 
        {
            _checkRoutine = StartCoroutine(CheckLoop());
        }

        private void CheckArea()
        {
            Collider[] targets = Physics.OverlapSphere(transform.position, _checkRange, _pickUpMask);

            for (int i = 0; i < targets.Length; i++)
            {
                Transform t = targets[i].transform;
                InventorySystem inventory;
                if (t.TryGetComponent<InventorySystem>(out inventory))
                {
                    if (inventory.ContainsItem(_activatable.IDRequired))
                    {
                        Debug.Log("Yep im here");
                        _activatable.Activate();
                        // We no longer need to run the check since it has been activated
                        StopCoroutine(_checkRoutine);
                    }
                }
            }
        }

        private IEnumerator CheckLoop()
        {
            while(true)
            {
                yield return _wait;
                CheckArea();
            }
        }
    }
}
