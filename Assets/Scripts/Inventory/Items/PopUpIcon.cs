﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpIcon : MonoBehaviour
{
    private Transform _mainCameraTransform;
    // Start is called before the first frame update
    private void Start()
    {
        _mainCameraTransform = Camera.main.transform;
    }

    // Update is called once per frame
    private void Update()
    {
        transform.LookAt(_mainCameraTransform);
    }
}
