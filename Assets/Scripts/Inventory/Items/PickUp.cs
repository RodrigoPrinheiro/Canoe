﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Inventory.Items
{
    [RequireComponent(typeof(FieldOfView))]
    public class PickUp : MonoBehaviour
    {
        [SerializeField] private LayerMask _pickUpMask;
        [SerializeField] private float _pickUpRadius;
        [SerializeField] private ItemUI _uiPrefab;
        private WaitForSeconds _pickUpDelay;
        private Item _currentItem;
        private FieldOfView _fov;
        private InventorySystem _playerInventory;
        private Dictionary<Transform, ItemUI> _activeItemsWithUI;
        public Action onItemPickUp;

        public Item CurrentItem
        {
            get
            {
                onItemPickUp?.Invoke();
                return _currentItem;
            }
        }

        public float Radius => _pickUpRadius;

        private void Awake()
        {
            _activeItemsWithUI = new Dictionary<Transform, ItemUI>();
            _pickUpDelay = new WaitForSeconds(1.2f);
            _fov = GetComponent<FieldOfView>();
            _playerInventory = GetComponent<InventorySystem>();
        }

        // Start is called before the first frame update
        void Start()
        {
            StartCoroutine(PickLoop());
        }

        private void CheckPickUp()
        {
            Collider[] targets = Physics.OverlapSphere(transform.position, _pickUpRadius, _pickUpMask);

            float dist = _pickUpRadius + 1;
            for (int i = 0; i < targets.Length; i++)
            {
                Transform t = targets[i].transform;
                float distance = (t.position - transform.position).magnitude;
                Item item;
                if (t.TryGetComponent<Item>(out item))
                {
                    if (dist > distance && _fov.Contains(t))
                    {
                        // If the current picked item is different than the item
                        // that was found, then add it to the inventory
                        if (_currentItem != item)
                        {
                            _currentItem = item;
                            if (!_playerInventory.ContainsItem(item))
                            {
                                _playerInventory.AddItem(item);
                                Destroy(_activeItemsWithUI[item.transform].gameObject);
                                _activeItemsWithUI.Remove(item.transform);
                                item.PickUp();
                                
                            }
                        }

                    }
                }
            }
        }

        private void ShowItemUI()
        {
            List<KeyValuePair<Transform, ItemUI>> keysToDelete = new List<KeyValuePair<Transform, ItemUI>>();
            
            // Check if there is any Key out of the view, if there is add it to the
            // delete keys array
            foreach(KeyValuePair<Transform, ItemUI> key in _activeItemsWithUI)
            {
                if (!_fov.ViewObjects.Contains(key.Key))
                {
                    keysToDelete.Add(key);
                    continue;
                }
            }

            for (int i = 0; i < keysToDelete.Count; i++)
            {
                _activeItemsWithUI.Remove(keysToDelete[i].Key);
                
                Destroy(keysToDelete[i].Value.gameObject);
                keysToDelete.RemoveAt(i);
            }

            for (int i = 0; i < _fov.ViewObjects.Count; i++)
            {
                if (_fov.ViewObjects[i].tag == "Passage") continue;
                if (!_activeItemsWithUI.ContainsKey(_fov.ViewObjects[i]))
                {
                    ItemUI ui = Instantiate(_uiPrefab, _fov.ViewObjects[i]);
                    _activeItemsWithUI.Add(_fov.ViewObjects[i], ui);
                }
            }
        }

        private IEnumerator PickLoop()
        {
            while (true)
            {
                yield return _pickUpDelay;
                ShowItemUI();
                CheckPickUp();
            }
        }
    }
}
