﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Inventory.Items
{
    [CreateAssetMenu(menuName = "Canoe/ItemInfo")]
    public class ItemInfo : ScriptableObject
    {
        [SerializeField] private GameObject _itemObject;
        public GameObject Object => _itemObject;
        [SerializeField] private int _id;
        public int ID => _id;

        [TextArea]
        [SerializeField] private string _description;
        public string Description => _description;
    }
}