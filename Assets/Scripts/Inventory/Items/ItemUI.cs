﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUI : MonoBehaviour
{
    private const float UI_HEIGHT = 2f;
    [SerializeField] private Transform _lineTransform;
    private PopUpIcon _active;
    private Transform _cameraTransform;
    private Transform _target;
    
    private void Awake() 
    {
        _cameraTransform = Camera.main.transform;    
    }

    public void NewTarget(Transform target)
    {
        _target = target;
    }

    private void Update()
    {
        Vector3 dir = _lineTransform.position - _cameraTransform.position;
        _lineTransform.rotation = Quaternion.LookRotation(dir, Vector3.up);

        if (_target)
        {
            transform.position = _target.position;
        }
    }

    public void Remove()
    {
        LeanTween.moveLocalY(_active.gameObject, -UI_HEIGHT, 0.6f).
        setOnComplete(KILL);

        void KILL()
        {
            Destroy(_active.gameObject);
        }
    }
}
