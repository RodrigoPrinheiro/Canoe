﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inventory.Items;

namespace Inventory
{
    public class InventorySystem : MonoBehaviour
    {
        [SerializeField] private Transform _inventorySpace;
        private List<Item> _inventory;
        private FieldOfView _playerView;
        private PickUp _pickUp;
        
        private void Awake()
        {
            _inventory = new List<Item>();
        }

        public void AddItem(Item item)
        {
            _inventory.Add(item);
            Debug.Log(item.ItemName);
            Instantiate(item.Info.Object, _inventorySpace);
        }

        public bool ContainsItem(Item item)
        {
            return _inventory.Contains(item);
        }

        public bool ContainsItem(int itemID)
        {
            foreach(Item i in _inventory)
            {
                if (i.Info.ID == itemID)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
