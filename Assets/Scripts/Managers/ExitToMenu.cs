﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitToMenu : MonoBehaviour
{
    private LoadSceneHelper _helper;
    // Start is called before the first frame update
    void Start()
    {
        _helper = GetComponent<LoadSceneHelper>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (_helper) _helper.Load();
        }
    }
}
