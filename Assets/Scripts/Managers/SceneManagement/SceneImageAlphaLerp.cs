﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SceneImageAlphaLerp : MonoBehaviour
{
    private Image _img;
    private Color _initialColor;
    [SerializeField] private float _speed = 1f;
    // Start is called before the first frame update
    void Start()
    {
        _img = GetComponent<Image>();
        _img.color = new Color(1, 1, 1, 0);
        _initialColor = _img.color;

        StartCoroutine(LerpColor());
    }

    private IEnumerator LerpColor()
    {
        while(_img.color.a < 0.9f)
        {
            _img.color = Color.Lerp(_img.color, Color.white, Time.deltaTime * _speed);
            yield return null;
        }

        while (_img.color.a > 0.01f)
        {
            _img.color = Color.Lerp(_img.color, _initialColor, Time.deltaTime * _speed);
            yield return null;
        }
    }
}
