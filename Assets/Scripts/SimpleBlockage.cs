﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Inventory.Items;

public class SimpleBlockage : MonoBehaviour, IItemActivatable
{
    [SerializeField] private int _itemIDRequired;

    public int IDRequired => _itemIDRequired;
    public UnityEngine.Events.UnityEvent onActivated;
    public void Activate()
    {
        onActivated?.Invoke();
        transform.LeanScale(Vector3.zero, 1.2f).setEaseOutBounce().
            setOnComplete(() => Destroy(gameObject));
    }
}
