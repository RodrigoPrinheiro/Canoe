﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FieldOfView))]
public class FieldOfViewInspector : Editor
{
    private void OnSceneGUI() 
    {
        FieldOfView fow = (FieldOfView)target;
        
        if (fow == null) return;
        
        Handles.color = Color.white;
        Handles.DrawWireArc(fow.transform.position, Vector3.up, Vector3.forward, 360, fow.Radius);
        
        Vector3 viewAngleA = fow.DirFromAngle(-fow.Angle / 2, false);
        Vector3 viewAngleB = fow.DirFromAngle(fow.Angle / 2, false);
        
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleA * fow.Radius);
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleB * fow.Radius);

        if (fow.ViewObjects != null && fow.ViewObjects.Count > 0)
            Handles.color = new Color(0, 1, 0, .2f);
        else
            Handles.color = new Color(1, 0, 0, .2f);

        Handles.DrawSolidArc(fow.transform.position, Vector3.up, viewAngleA, fow.Angle, fow.Radius);
        Handles.color = Color.red;
        if (fow.ViewObjects != null && fow.ViewObjects.Count > 0)
            foreach(Transform t in fow.ViewObjects)
            {
                if (t == null) continue;
                Handles.DrawLine(fow.transform.position, t.transform.position);
            }
    }
}
