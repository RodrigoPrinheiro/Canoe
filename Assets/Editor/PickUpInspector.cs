using UnityEditor;
using UnityEngine;
using Inventory.Items;

[CustomEditor(typeof(PickUp))]
public class PickUpInspector : Editor
{
    private void OnSceneGUI() 
    {
        PickUp p = (PickUp)target;
        
        if (p == null) return;

        Handles.color = new Color(0, 0, 1, .2f);
        Handles.DrawSolidArc(p.transform.position, Vector3.up, Vector3.forward, 360,p.Radius);
        Handles.color = Color.white;
        Handles.DrawWireArc(p.transform.position, Vector3.up, Vector3.forward, 360, p.Radius);
    }
}