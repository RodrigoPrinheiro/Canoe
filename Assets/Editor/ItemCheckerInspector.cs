using UnityEditor;
using UnityEngine;
using Inventory.Items;

[CustomEditor(typeof(ItemChecker))]
public class ItemCheckerInspector : Editor
{
    private void OnSceneGUI() 
    {
        ItemChecker p = (ItemChecker)target;
        
        if (p == null) return;

        Handles.color = new Color(0, 0, 1, .2f);
        Handles.DrawSolidArc(p.transform.position, Vector3.up, Vector3.forward, 360,p.Radius);
        Handles.color = Color.white;
        Handles.DrawWireArc(p.transform.position, Vector3.up, Vector3.forward, 360, p.Radius);
    }
}