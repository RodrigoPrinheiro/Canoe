#include <Arduino.h>
#include <Button.h>
#include <Keyboard.h>

// Pin Definitions
#define MICROSWITCH_1_PIN_COM  2
#define MICROSWITCH_2_PIN_COM 3
#define MICROSWITCH_3_PIN_COM 4
#define MICROSWITCH_4_PIN_COM 5


// object initialization
Button microSwitch_1(MICROSWITCH_1_PIN_COM);
Button microSwitch_2(MICROSWITCH_2_PIN_COM);
Button microSwitch_3(MICROSWITCH_3_PIN_COM);
Button microSwitch_4(MICROSWITCH_4_PIN_COM);

char move = 'W';
char rotate = 32; // space bar

void setup() 
{
  // Setup Serial which is useful for debugging
  Serial.begin(9600);
  while (!Serial) ; // wait for serial port to connect. Needed for native USB
  Keyboard.begin();
  Serial.println("start");
  // Setup micro switches
  microSwitch_1.begin();
  microSwitch_2.begin();
  microSwitch_3.begin();
  microSwitch_4.begin();
}

void loop()
{
  // Check for the first pair of buttons that correspond to moving in the game
  
  if (microSwitch_1.read() == Button::PRESSED || 
    microSwitch_2.read() == Button::PRESSED)
  {
    Keyboard.press(move);
    Serial.println("W is pressed");
  }
  else
  {
    Keyboard.release(move);
    Serial.println("Released W");
  }

  if (microSwitch_3.read() == Button::PRESSED || 
    microSwitch_4.read() == Button::PRESSED)
  {
    Keyboard.press(rotate);
    Serial.println("Space is pressed");
  }
  else
  {
    Keyboard.release(rotate);
    Serial.println("Released Space");
  }
}
